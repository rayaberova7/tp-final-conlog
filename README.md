# TP final de conception de logiciel
Dans ce TP final, nous avons créé cette application qui permet de faire des playlists aléatoires basées sur les artistes préférés de votre choix ainsi que les notes sur 20 que vous leurs attribuez en fonction de vos préférences. Il suffit de mettre en entrée un fichier json avec le nom de vos artistes et les notes que vous leurs attribuez. C'est idéal en soirée !!

## Schéma d'architecture

```mermaid
graph TD
A[client] --> B[serveur]
B --> C[AudioDB API]
B -->D[LyricsOVH API]
```

## Pour commencer
Il faut d'abord installer le git de l'application.

````
git clone https://gitlab.com/rayaberova7/tp-final-conlog.git
cd tp-final-conlog
pip install -r requirements.txt
````
## Lancer le serveur
Il faut que le serveur soit lancé avant de créer une playlist. 

````
cd serveur
python3 main.py
````

## Créer votre playlist
Faire un fichier json avec les noms des artistes que vous souhaitez écouter ainsi que la notation que vous leur attribuez. 
Il doit contenir une liste de dictionnaires avec les deux clés "artiste" et "note". Mettre le nom de ce fichier json dans le .env à la place de "rudy.json".

```
cd ..
cd client
python3 main.py
```

## Test unitaire

On peut lancer des tests unitaires ainsi qu'un test prenant en compte deux fonctionnalités pour voir que l'application fonctionne bien.

```
cd ..
cd test
python3 unit_test.py
```
