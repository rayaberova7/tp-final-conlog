import random
import requests
import json
from asyncore import write
from dotenv import load_dotenv
from dotenv import dotenv_values
import os


def get_Playlist(file_name):
    '''
    get_Playlist returns a random playlist by preferences from a json file with artists's names and ratings. 
    The more an artist has a high rating, the more he occures in the playlist. It also create a json file with the playlist in it.

    Arguments :
        str : json file name

    Retour :
        list : Liste de dictionnaire contenant des informatiosn diverses sur chaque musique générés aléatoirement.
    '''

    with open(file_name) as file:
        list_artist = json.load(file)

    names = []
    ratings = []
    for artist in list_artist:
        names.append(artist["artiste"])
        ratings.append(artist["note"])

    artist_preferances = random.choices(names, weights=ratings, k=20)

    playlist = []
    for artist in artist_preferances:
        config = dotenv_values("../.env")
        response = requests.get(config["URL"] + "/random/" + artist)
        playlist.append(response.json())

    with open('playlist.json', 'w') as playlist_file:
        json.dump(playlist, playlist_file, indent=4)

    return(playlist)

if __name__ == "__main__":
    load_dotenv()
    playlist = get_Playlist(os.getenv("file_name"))
