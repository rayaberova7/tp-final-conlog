from fastapi import FastAPI
import uvicorn
import requests
import random

app = FastAPI()

def get_IdArtist(name_artist: str):
    '''
    get_IdArtist returns artist's identifiant from artist's name, if the artist is recognized.

    Arguments :
    str : artist name

    Returns :
    int : artist identifiant

    Example :
    >>> get_IdArtist('coldplay')
    111239
    '''
    new_nameartist = name_artist.replace(' ', '_')
    try :
        url = "https://www.theaudiodb.com/api/v1/json/2/search.php?s=" + str(new_nameartist)
        response = requests.get(url)
        id_artist = response.json()['artists'][0]['idArtist']
        return(id_artist)
    except :
        print("Artist not found.")

def get_IdRandomAlbum(name_artist: str):
    '''
    get_IdRandomAlbum returns identifiant of a random album from artist's name, if the artist is recognized.

    Arguments :
    str : artist name

    Returns :
    int : random album identifiant

    Example :
    >>> get_IdRandomAlbum('the_weeknd')
    2115886
    '''
    id_artist = get_IdArtist(name_artist)
    url = 'theaudiodb.com/api/v1/json/2/album.php?i=' + str(id_artist)
    response = requests.get(url)
    albums = response.json()['album']
    random_album = random.randint(0, len(albums)-1)
    id_random_album = response.json()['album'][random_album]['idAlbum']
    return(id_random_album)

def get_IdRandomTrack(name_artist : str):
    '''
    get_IdRandomTrack returns random track identifiant from artist name if the artist is recognized. 

    Arguments :
    str : artist name

    Returns :
    int : random track identifiant

    Example :
    >>> get_IdRandomTrack('the_weeknd')
    32793500
    '''
    id_album = get_IdRandomAlbum(name_artist)
    url = 'theaudiodb.com/api/v1/json/2/track.php?m=' + str(id_album)
    response = requests.get(url)
    tracks = response.json()['track']
    random_track = random.randint(0, len(tracks)-1)
    id_random_track = response.json()['track'][random_track]['idTrack']
    return(id_random_track) 

def get_TrackName(id_track: int):
    '''
    get_TrackName returns track's name from track's identifiant.

    Arguments :
    int : track identifiant

    Returns : 
    str : track name

    Example :
    >>> get_TrackName(32793500)
    D.D.
    '''
    url = 'theaudiodb.com/api/v1/json/2/track.php?h=' + str(id_track)
    response = requests.get(url)
    name_track = response.json()['track'][0]['strTrack']
    return(name_track)

def get_YtVideo(id_track: int):
    '''
    get_YtVideo returns track's youtube url from track's identifiant.

    Arguments :
    int : track identifiant

    Returns :
    url : track youtube video url 

    Example :
    >>> get_YtTrack(32793491)
    http://www.youtube.com/watch?v=JDe86ul6RmI
    '''
    url1 = 'theaudiodb.com/api/v1/json/2/track.php?h=' + str(id_track)
    response1 = requests.get(url1)
    id_artist = response1.json()['track'][0]['idArtist']
    url2 = 'theaudiodb.com/api/v1/json/2/mvid.php?i=' + str(id_artist)
    reponse2 = requests.get(url2)
    youtube_videos = reponse2.json()['mvids']
    for i in range(len(youtube_videos)): 
        id_track_video = reponse2.json()['mvids'][i]['idTrack']
        if id_track_video == id_track:
            id_video = i
    youtube_video = reponse2.json()['mvids'][id_video]['strMusicVid']
    return(youtube_video)

def get_lyrics(name_artist, name_track):
    ''' 
    get_lyrics returns lyrics from artist's name and track's name, if they are recognized.

    Arguments :
        - str : artist name
        - str : artist name

    Returns :
    str : lyrics
    '''
    try :
        new_nameartist = name_artist.replace(' ','%20')
        new_nametrack = name_track.replace(' ','%20')
        url = 'https://api.lyrics.ovh/v1/' + str(new_nameartist) + str(new_nametrack)
        response = requests.get(url)
        lyrics = response.json()['lyrics']
        return(lyrics)
    except :
        print("Artist or Track not found.")

def healthcheck_lyrics():
    request = requests.get('https://api.lyrics.ovh/v1/The%20Weeknd/High%20for%20this')
    print(request.status_code)


def healthcheck_audioDB():
    request = requests.get('https://www.theaudiodb.com/api/v1/json/2/search.php?s=the_weeknd')
    print(request.status_code)



@app.get("/")
def read_root():
    test_lyrics = healthcheck_lyrics()
    test_audioDB = healthcheck_audioDB()
    return{'Status Lyricsovh': test_lyrics, 'Status AudioDB': test_audioDB}


@app.get("/random/{name_artist}")
def get_suggestion(name_artist: str):
    '''
    get_suggestion returns informations on a random track from a choosen artist.

    Arguments :
        str : artist name

    Retour :
        dic : dictionary containing artist's name, name of a random track, track's youtube url and track's lyrics
    '''
    id_randomtrack = get_IdRandomTrack(name_artist)
    name_track = get_TrackName(id_randomtrack)
    youtube_vids = get_YtVideo(id_randomtrack)
    lyrics = get_lyrics(name_artist, name_track)
    suggestion = {"artist": name_artist, "track": name_track, "suggested_youtube_url": get_YtVideo(id_track), "lyrics": lyrics}
    return(suggestion)


if __name__ == "__main__":
    uvicorn.run(app)

