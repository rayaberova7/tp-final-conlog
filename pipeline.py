

class Pipeline:
    
    """Classe qui créé une pile d'opérartions étape par étape puis l'applique sur une un objet.
    
    Attribut
    ----------
    operations : list = []
        la pile de toutes les opérations dans l'ordre qui sera appliquée sur l'objet


    Méthodes
    ----------
    add_step() 
        ajoute une opération à faire dans la pile
    
    execute()
        applique la pile d'opérations sur la table

    
    """
    def __init__(self):
        """
        Attribut
        ----------
        operations : list = []
            pile initialement vide
        
        """
        self.operations = []
    
    def add_step(self,step):
        """Ajoute une opération à faire dans la pile

        Attribut
        ----------
        step : Operation
            operation à ajouter dans la pile d'opérations
        """
        self.operations.append(step)

    def run(self,object):
        """Applique la pile d'opérations sur l'objet.
        
        Attribut
        ----------
        object
            l'objet sur laquelle on va appliquer toutes les opérations souhaitées

        Retour
        -------
        Retourne le resultat apès que toutes les opérations ont été appliquées
        
        """     
        n=len(self.operations)
        for k in range(n) :
            result = self.operations[-1].execute(object)
            self.operations.pop()
        return result
